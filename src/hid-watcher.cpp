#include "../include/hid-watcher.hpp"

namespace hidcpp
{
HidWatcher &HidWatcher::getInstance()
{
	static HidWatcher instance;
	static std::once_flag flag;
	std::call_once(flag, []() {
		instance.start();

		std::atexit([]() {
			instance.stop();
		});
	});
	return instance;
}

HidWatcher::CallbackId HidWatcher::addCallback(const std::function<bool(const std::string &path, bool removed)> &callback)
{
	return pImpl->addCallback(callback);
}

void HidWatcher::removeCallback(CallbackId id)
{
	return pImpl->removeCallback(id);
}

HidResult HidWatcher::start()
{
	return pImpl->start();
}

HidResult HidWatcher::stop()
{
	return pImpl->stop();
}
} // namespace hidcpp