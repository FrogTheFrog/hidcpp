#include "../include/hid-device.hpp"
#include <string>

namespace hidcpp
{
HidResult HidDevice::open(const std::string &path, bool closeIfOpen, const std::optional<std::function<void(const std::string &path)>> &onClose)
{
	return pImpl->open(path, closeIfOpen, onClose);
}

HidResult HidDevice::close()
{
	return pImpl->close();
}

std::optional<DeviceInfo> HidDevice::getDeviceInfo() const
{
	return pImpl->getDeviceInfo();
}

HidResult HidDevice::abortReading()
{
	return pImpl->abortReading();
}

HidResult HidDevice::read(const std::variant<std::reference_wrapper<DataArray>, std::function<bool(const DataArray &)>> &outputOrCallback, const std::optional<std::chrono::milliseconds> &timeout)
{
	return pImpl->read(outputOrCallback, timeout);
}

HidResult HidDevice::write(const DataArray &data)
{
	return pImpl->write(data);
}

HidResult HidDevice::getFeatureReport(DataArray &output)
{
	return pImpl->getFeatureReport(output);
}

HidResult HidDevice::setFeatureReport(const DataArray &data)
{
	return pImpl->setFeatureReport(data);
}
} // namespace hidcpp