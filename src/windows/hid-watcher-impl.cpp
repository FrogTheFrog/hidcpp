#include "../include/hid-watcher.hpp"
#include "../include/scope.hpp"
#include <Windows.h>
#include <condition_variable>
#include <dbt.h>

namespace hidcpp::internals
{
class HidWatcherWindows : public HidWatcherImpl
{
  public:
	HidWatcherWindows() : handle{NULL}, idCounter{0}
	{
	}

	~HidWatcherWindows()
	{
		this->stop();
	}

  protected:
	HidResult HidWatcherWindows::start()
	{
		std::unique_lock<std::mutex> lock(this->mutex);
		std::unique_lock<std::mutex> threadLock(this->threadMutex);

		std::exception_ptr threadException;
		HidResult result;

		if (this->isWatching())
		{
			return errc::OK;
		}

		// Windows handle must be created in thread in order
		// to receive all messages from message pump.
		std::thread([this, &threadException, &result]() {
			try
			{
				std::unique_lock threadLock(this->threadMutex);
				const std::string className{"HidCpp_HidWatcher"};

				sr::unique_resource watcherClass{
					WNDCLASSEXA{0},
					[](auto &&data) { if (data.cbSize != 0) UnregisterClassA(data.lpszClassName, data.hInstance); }};
				sr::unique_resource notificationHandle{
					static_cast<HDEVNOTIFY>(NULL),
					[](auto &&data) { if (data != NULL) UnregisterDeviceNotification(data); }};

				MSG msg{0};
				DWORD error{0};
				DEV_BROADCAST_DEVICEINTERFACE filter{0};

				const_cast<WNDCLASSEXA &>(watcherClass.get()).cbSize = sizeof(WNDCLASSEXA);
				const_cast<WNDCLASSEXA &>(watcherClass.get()).lpfnWndProc = &HidWatcherWindows::messageHandler;
				const_cast<WNDCLASSEXA &>(watcherClass.get()).hInstance = GetModuleHandleA(NULL);
				const_cast<WNDCLASSEXA &>(watcherClass.get()).lpszClassName = className.c_str();

				if (!RegisterClassExA(&watcherClass.get()))
				{
					const_cast<WNDCLASSEXA &>(watcherClass.get()).cbSize = 0;
					result = GetLastError();
					return;
				}

				this->handle = CreateWindowExA(
					NULL,
					className.c_str(),
					className.c_str(),
					0, 0, 0, 0, 0,
					HWND_MESSAGE,
					NULL,
					GetModuleHandleA(NULL),
					NULL);

				if (this->handle == NULL)
				{
					result = GetLastError();
					return;
				}

				SetLastError(0);
				SetWindowLongPtrA(this->handle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
				error = GetLastError();

				if (error != 0)
				{
					result = GetLastError();
					return;
				}

				filter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
				filter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;

				/* Windows class GUID for HID devices. */
				filter.dbcc_classguid = {0x4d1e55b2, 0xf16f, 0x11cf, {0x88, 0xcb, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30}};

				notificationHandle.reset(
					RegisterDeviceNotificationA(this->handle, &filter, DEVICE_NOTIFY_WINDOW_HANDLE));

				if (notificationHandle.get() == NULL)
				{
					result = GetLastError();
					return;
				}

				threadLock.unlock();
				this->threadWait.notify_one();

				while (GetMessageA(&msg, this->handle, 0, 0) > 0)
				{
					TranslateMessage(&msg);
					DispatchMessageA(&msg);
				}

				threadLock.lock();
				this->handle = NULL;
				this->threadWait.notify_one();
			}
			catch (...)
			{
				threadException = std::current_exception();
				this->handle = NULL;
				threadWait.notify_one();
			}
		})
			.detach();

		this->threadWait.wait(threadLock);
		if (threadException)
		{
			std::rethrow_exception(threadException);
		}

		return result;
	}

	HidResult HidWatcherWindows::stop()
	{
		std::unique_lock<std::mutex> lock(this->mutex);
		std::unique_lock<std::mutex> threadLock(this->threadMutex);

		if (!this->isWatching())
		{
			return errc::OK;
		}

		PostMessageA(this->handle, WM_CLOSE, 0, 0);
		this->threadWait.wait(threadLock);

		return errc::OK;
	}

	CallbackId addCallback(const std::function<bool(const std::string &path, bool removed)> &callback)
	{
		std::scoped_lock lock(this->mutex, this->callbackMutex);
		return this->addCallbackUnlocked(callback);
	}

	void removeCallback(CallbackId id)
	{
		std::scoped_lock lock(this->mutex, this->callbackMutex);
		return this->removeCallbackUnlocked(id);
	}

  private:
	std::mutex threadMutex, callbackMutex;
	std::condition_variable threadWait;
	HWND handle;
	CallbackId idCounter;

	static LRESULT CALLBACK messageHandler(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT onMessage(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_DEVICECHANGE:
		{
			decltype(this->callbacks) callbacks;
			{
				std::unique_lock lock(this->callbackMutex);
				callbacks = this->callbacks;
			}

			if (callbacks.size() > 0 && (wParam == DBT_DEVICEARRIVAL || wParam == DBT_DEVICEREMOVECOMPLETE))
			{
				std::vector<CallbackId> idsToRemove;

				PDEV_BROADCAST_HDR broadcast = reinterpret_cast<PDEV_BROADCAST_HDR>(lParam);
				if (broadcast->dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
				{
					PDEV_BROADCAST_DEVICEINTERFACE_A deviceInterface = reinterpret_cast<PDEV_BROADCAST_DEVICEINTERFACE_A>(broadcast);
					std::string path(deviceInterface->dbcc_name);
					bool removed = wParam == DBT_DEVICEREMOVECOMPLETE;

					for (auto [id, callback] : callbacks)
					{
						if (callback(path, removed))
						{
							idsToRemove.push_back(id);
						}
					}

					if (idsToRemove.size() > 0)
					{
						std::unique_lock lock(this->callbackMutex);
						for (auto id : idsToRemove)
						{
							this->removeCallbackUnlocked(id);
						}
					}
				}
				break;
			}
		}
		default:
			break;
		}
		return DefWindowProcA(hwnd, uMsg, wParam, lParam);
	}

	bool isWatching() const
	{
		return this->handle != NULL;
	}

	CallbackId addCallbackUnlocked(const std::function<bool(const std::string &path, bool removed)> &callback)
	{
		auto id = this->idCounter++;
		this->callbacks[id] = callback;

		return id;
	}

	void removeCallbackUnlocked(CallbackId id)
	{
		this->callbacks.erase(id);
	}
};

LRESULT HidWatcherWindows::messageHandler(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HidWatcherWindows *watcher = reinterpret_cast<HidWatcherWindows *>(GetWindowLongPtrA(hwnd, GWLP_USERDATA));
	return watcher->onMessage(hwnd, uMsg, wParam, lParam);
}
} // namespace hidcpp::internals

namespace hidcpp
{
HidWatcher::HidWatcher() : pImpl{std::make_unique<internals::HidWatcherWindows>()}
{
}
} // namespace hidcpp
