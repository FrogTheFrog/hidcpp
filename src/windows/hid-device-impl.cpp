#include "../../include/hid-device.hpp"
#include "../../include/scope.hpp"
#include "../../include/thread-safe-ptr.hpp"
#include "./include/hid-dll-loader.hpp"
#include <algorithm>
#include <array>
#include <atomic>
#include <cctype>
#include <charconv>

#include <Windows.h>

#include <Hidclass.h>
#include <Setupapi.h>

#pragma comment(lib, "Setupapi.lib")

namespace hidcpp
{
HidResult HidDevice::getDeviceStatus(const std::string &path)
{
	sr::unique_resource deviceHandle{
		CreateFileA(
			path.c_str(),
			NULL,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0),
		[](auto &&data) { if (data != INVALID_HANDLE_VALUE) CloseHandle(data); }};

	if (deviceHandle.get() == INVALID_HANDLE_VALUE)
	{
		return GetLastError();
	}

	return errc::OK;
}

tl::expected<DeviceInfo, HidResult> HidDevice::generateDeviceInfo(const std::string &path)
{
	DeviceInfo deviceInfo;
	internals::HidDllLoader &dll = internals::HidDllLoader::get();

	/* A device handle to enumerated device which is used
		to extract further data about HID device.*/
	sr::unique_resource deviceHandle{
		CreateFileA(
			path.c_str(),
			NULL,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0),
		[](auto &&data) { if (data != INVALID_HANDLE_VALUE) CloseHandle(data); }};

	/* Preparsed data pointer (used to get hid capabilities). */
	sr::unique_resource preparsedData{
		static_cast<internals::HidDllLoader::PHIDP_PREPARSED_DATA>(nullptr),
		[&dll](auto &&data) { if (data) dll.HidD_FreePreparsedData(data); }};

	/* HID attribute structure (for vendorID, productID and versionNumber). */
	internals::HidDllLoader::HIDD_ATTRIBUTES hidAttributes{0};
	hidAttributes.size = sizeof(internals::HidDllLoader::HIDD_ATTRIBUTES);

	/* Buffer for retrieving various NULL terminated strings from OS. */
	WCHAR wideBuffer[512] = {0};

	/* Hid capabilities (usage and usagePage). */
	internals::HidDllLoader::HIDP_CAPS hidCapabilities;

	/* Check whether device handle is invalid. */
	if (deviceHandle.get() == INVALID_HANDLE_VALUE)
	{
		return tl::unexpected(GetLastError());
	}

	/* Save device path. */
	deviceInfo.path = path;

	/* Try to retrieve productId, vendorId and versionNumber. */
	BOOL result = dll.HidD_GetAttributes(deviceHandle.get(), &hidAttributes);
	if (result)
	{
		deviceInfo.productId = hidAttributes.productId;
		deviceInfo.vendorId = hidAttributes.vendorId;
		deviceInfo.versionNumber = hidAttributes.versionNumber;
	}
	else
	{
		return tl::unexpected(GetLastError());
	}

	/* Try to retrieve usagePage and usage. */
	result = dll.HidD_GetPreparsedData(deviceHandle.get(), &const_cast<internals::HidDllLoader::PHIDP_PREPARSED_DATA>(preparsedData.get()));
	if (result)
	{
		constexpr NTSTATUS HIDP_STATUS_SUCCESS = 0x110000;

		if (dll.HidP_GetCaps(preparsedData.get(), &hidCapabilities) == HIDP_STATUS_SUCCESS)
		{
			deviceInfo.usagePage = hidCapabilities.usagePage;
			deviceInfo.usage = hidCapabilities.usage;
			deviceInfo.inputReportLength = hidCapabilities.inputReportByteLength;
			deviceInfo.outputReportLength = hidCapabilities.outputReportByteLength;
			deviceInfo.featureReportLength = hidCapabilities.featureReportByteLength;
		}
		else
		{
			return tl::unexpected(GetLastError());
		}
	}
	else
	{
		return tl::unexpected(GetLastError());
	}

	/* Try to retrieve serialNumber. */
	result = dll.HidD_GetSerialNumberString(deviceHandle.get(), wideBuffer, sizeof(wideBuffer));
	if (result)
	{
		auto requiredSize = WideCharToMultiByte(CP_UTF8, NULL, wideBuffer, -1, nullptr, 0, NULL, NULL);
		if (requiredSize > 0)
		{
			deviceInfo.serialNumber = std::string(--requiredSize, 0);
			WideCharToMultiByte(CP_UTF8, NULL, wideBuffer, requiredSize, deviceInfo.serialNumber->data(), requiredSize, NULL, NULL);
		}
	}

	/* Try to retrieve manufacturer. */
	result = dll.HidD_GetManufacturerString(deviceHandle.get(), wideBuffer, sizeof(wideBuffer));
	if (result)
	{
		auto requiredSize = WideCharToMultiByte(CP_UTF8, NULL, wideBuffer, -1, nullptr, 0, NULL, NULL);
		if (requiredSize > 0)
		{
			deviceInfo.manufacturer = std::string(--requiredSize, 0);
			WideCharToMultiByte(CP_UTF8, NULL, wideBuffer, requiredSize, deviceInfo.manufacturer->data(), requiredSize, NULL, NULL);
		}
	}

	/* Try to retrieve product. */
	result = dll.HidD_GetProductString(deviceHandle.get(), wideBuffer, sizeof(wideBuffer));
	if (result)
	{
		auto requiredSize = WideCharToMultiByte(CP_UTF8, NULL, wideBuffer, -1, nullptr, 0, NULL, NULL);
		if (requiredSize > 0)
		{
			deviceInfo.product = std::string(--requiredSize, 0);
			WideCharToMultiByte(CP_UTF8, NULL, wideBuffer, requiredSize, deviceInfo.product->data(), requiredSize, NULL, NULL);
		}
	}

	/* Try to extract interface. */
	constexpr std::string_view interfaceString = "&mi_";
	auto it = std::search(
		deviceInfo.path.begin(), deviceInfo.path.end(),
		interfaceString.begin(), interfaceString.end(),
		[](auto a, auto b) { return std::toupper(a) == std::toupper(b); });

	if (it != deviceInfo.path.end())
	{
		uint16_t value;
		auto result = std::from_chars(&*(it + 4), &*(it + 6), value);
		if (result.ec != std::errc::invalid_argument && result.ec != std::errc::result_out_of_range)
		{
			deviceInfo.interfaceNumber = value;
		}
	}

	return std::move(deviceInfo);
}

tl::expected<EnumerationData, HidResult> HidDevice::enumerate()
{
	return std::move(HidDevice::enumerate([](auto &&) { return true; }));
}

tl::expected<EnumerationData, HidResult> HidDevice::enumerate(const std::function<bool(const std::string &)> &predicate)
{
	EnumerationData enumeratedData;
	internals::HidDllLoader &dll = internals::HidDllLoader::get();

	/* Windows class GUID for HID devices. */
	GUID classGuid = {0x4d1e55b2, 0xf16f, 0x11cf, {0x88, 0xcb, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30}};
	SP_DEVINFO_DATA devinfoData{0};
	SP_DEVICE_INTERFACE_DATA deviceInterfaceData{0};
	sr::unique_resource deviceInfoSet{
		static_cast<HDEVINFO>(INVALID_HANDLE_VALUE),
		[](auto &&data) { if (data != INVALID_HANDLE_VALUE) SetupDiDestroyDeviceInfoList(data); }};

	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
	devinfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	/* Get information for all the devices belonging to the HID class. */
	deviceInfoSet.reset(SetupDiGetClassDevsA(&classGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

	if (deviceInfoSet.get() == INVALID_HANDLE_VALUE)
	{
		return tl::unexpected(GetLastError());
	}

	for (size_t deviceIndex = 0;; deviceIndex++)
	{
		DWORD deviceInterfaceDetailSize = 0;

		/* Heap allocated interface detail structure. */
		sr::unique_resource deviceInterfaceDetailData{
			static_cast<SP_DEVICE_INTERFACE_DETAIL_DATA_A *>(nullptr),
			[](auto &&data) { delete[] data; }};

		/* Get device interface data by the index. */
		BOOL result = SetupDiEnumDeviceInterfaces(
			deviceInfoSet.get(),
			NULL,
			&classGuid,
			static_cast<DWORD>(deviceIndex),
			&deviceInterfaceData);

		if (!result)
		{
			if (GetLastError() != ERROR_NO_MORE_ITEMS)
			{
				return tl::unexpected(GetLastError());
			}
			else
			{
				break;
			}
		}

		/* Retrieve size of detail structure. */
		result = SetupDiGetDeviceInterfaceDetailA(
			deviceInfoSet.get(),
			&deviceInterfaceData,
			NULL,
			0,
			&deviceInterfaceDetailSize,
			NULL);

		if (!result && GetLastError() != ERROR_INSUFFICIENT_BUFFER)
		{
			return tl::unexpected(GetLastError());
		}
		else if (deviceInterfaceDetailSize == 0)
		{
			break;
		}

		/* Resize structure to retrieved size. */
		deviceInterfaceDetailData.reset(
			reinterpret_cast<SP_DEVICE_INTERFACE_DETAIL_DATA_A *>(new uint8_t[deviceInterfaceDetailSize]));
		deviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA_A);

		/* Retrieve the actual interface detail structure. */
		result = SetupDiGetDeviceInterfaceDetailA(
			deviceInfoSet.get(),
			&deviceInterfaceData,
			deviceInterfaceDetailData.get(),
			deviceInterfaceDetailSize,
			NULL,
			NULL);

		if (!result)
		{
			return tl::unexpected(GetLastError());
		}

		/* Make sure device is "HIDClass" and has a driver bound to it. */
		auto deviceIsValid = [&deviceInfoSet, &devinfoData, &result]() -> tl::expected<bool, HidResult> {
			bool isValid = false;
			std::array<char, 256> driverName;

			for (size_t infoIndex = 0; !isValid; ++infoIndex)
			{
				std::fill(driverName.begin(), driverName.end(), 0);

				/* Retrieve device information data. */
				result = SetupDiEnumDeviceInfo(deviceInfoSet.get(), static_cast<DWORD>(infoIndex), &devinfoData);

				/* Check if device info data was retrieved successfully. */
				if (!result)
				{
					if (GetLastError() != ERROR_NO_MORE_ITEMS)
					{
						return tl::unexpected(GetLastError());
					}
					break;
				}

				/* Retrieve driver name. */
				result = SetupDiGetDeviceRegistryPropertyA(
					deviceInfoSet.get(),
					&devinfoData,
					SPDRP_CLASS,
					NULL,
					reinterpret_cast<PBYTE>(driverName.data()),
					sizeof(driverName),
					NULL);

				/* Check if device has driver name, skip device otherwise. */
				if (!result)
				{
					if (GetLastError() != ERROR_INVALID_DATA)
					{
						return tl::unexpected(GetLastError());
					}
					else
					{
						continue;
					}
				}

				if (std::strcmp(driverName.data(), "HIDClass") == 0)
				{
					/* Check if there's a driver bound. */
					result = SetupDiGetDeviceRegistryPropertyA(
						deviceInfoSet.get(),
						&devinfoData,
						SPDRP_DRIVER,
						NULL,
						reinterpret_cast<PBYTE>(driverName.data()),
						sizeof(driverName),
						NULL);

					/* Check if result is valid. */
					if (result)
					{
						isValid = true;
					}
					else if (GetLastError() != ERROR_INVALID_DATA)
					{
						return tl::unexpected(GetLastError());
					}
				}
			}
			return isValid;
		}();

		std::string path{deviceInterfaceDetailData->DevicePath};

		if (!deviceIsValid)
		{
			enumeratedData.errored.push_back({std::move(path), deviceIsValid.error()});
		}
		else if (deviceIsValid.value() && predicate(path))
		{
			enumeratedData.valid.push_back(std::move(path));
		}
	}

	return std::move(enumeratedData);
}
} // namespace hidcpp

namespace hidcpp::internals
{
class HidDeviceWindows : public HidDeviceImpl, public ThreadSafePtr<HidDeviceWindows>
{
  public:
	HidDeviceWindows()
		: readIsPending{false}, deviceHandle{INVALID_HANDLE_VALUE, [](void *data) { if (data != INVALID_HANDLE_VALUE) CloseHandle(data); }}, olRead{OVERLAPPED{0}, [](const OVERLAPPED &data) { if (data.hEvent != NULL) CloseHandle(data.hEvent); }}, olWrite{OVERLAPPED{0}, [](const OVERLAPPED &data) { if (data.hEvent != NULL) CloseHandle(data.hEvent); }}, olSet{OVERLAPPED{0}, [](const OVERLAPPED &data) { if (data.hEvent != NULL) CloseHandle(data.hEvent); }}, olGet{OVERLAPPED{0}, [](const OVERLAPPED &data) { if (data.hEvent != NULL) CloseHandle(data.hEvent); }}
	{
		const_cast<OVERLAPPED &>(this->olRead.get()).hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		const_cast<OVERLAPPED &>(this->olWrite.get()).hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		const_cast<OVERLAPPED &>(this->olSet.get()).hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		const_cast<OVERLAPPED &>(this->olGet.get()).hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	}

	~HidDeviceWindows()
	{
		this->dereference();
		this->close();
	}

	HidResult open(const std::string &path, bool closeIfOpen, const std::optional<std::function<void(const std::string &path)>> &onClose)
	{
		std::unique_lock lock(this->mutex);
		std::optional<std::string> oldPath = this->deviceInfo ? std::optional<std::string>{this->deviceInfo->path} : std::nullopt;

		auto [result, oldCallback] = this->openUnlocked(path, closeIfOpen, onClose);
		if (result)
		{
			auto &&watcher = HidWatcher::getInstance();
			auto callbackId = watcher.addCallback([safePtr = this->getSafePtr()](const std::string &path, bool removed) {
				auto self = safePtr->lock();
				if (self)
				{
					auto info = self->getDeviceInfo();
					if (info.has_value() && info->path == path)
					{
						self->close();
						return true;
					}
					return false;
				}
				return true;
			});

			result = HidDevice::getDeviceStatus(path);
			if (!result)
			{
				watcher.removeCallback(callbackId);
				this->closeUnlocked();
			}
			else
			{
				this->callbackId = callbackId;
			}
		}

		lock.unlock();

		if (oldPath && oldCallback)
		{
			oldCallback(oldPath.value());
		}
		if (!result && onClose)
		{
			(*onClose)(path);
		}

		return result;
	};

	HidResult close()
	{
		std::unique_lock lock(this->mutex);
		std::optional<std::string> path = this->deviceInfo ? std::optional<std::string>{this->deviceInfo->path} : std::nullopt;

		auto [result, callback] = this->closeUnlocked();
		if (path && callback)
		{
			callback(path.value());
		}

		return result;
	}

	std::optional<DeviceInfo> getDeviceInfo() const
	{
		std::lock_guard lock(this->mutex);
		return this->deviceInfo;
	};

	HidResult abortReading()
	{
		std::lock_guard lock(this->mutex);
		std::unique_lock readLock(this->readMutex, std::defer_lock);

		if (!readLock.try_lock())
		{
			if (!CancelIoEx(this->deviceHandle.get(), &const_cast<OVERLAPPED &>(this->olRead.get())))
			{
				DWORD error = GetLastError();
				if (error != ERROR_OPERATION_ABORTED)
				{
					return error;
				}
			}
		}

		return errc::OK;
	}

	HidResult read(const std::variant<std::reference_wrapper<DataArray>, std::function<bool(const DataArray &)>> &outputOrCallback, const std::optional<std::chrono::milliseconds> &timeout = std::nullopt)
	{
		std::unique_lock readLock(this->readMutex, std::defer_lock);

		{
			std::lock_guard lock(this->mutex);

			if (!this->isOpen())
			{
				return errc::DEVICE_IS_CLOSED;
			}
			else if (!readLock.try_lock())
			{
				return errc::BUSY;
			}
		}

		DataArray dataBuffer;
		BOOL result = FALSE;
		DWORD readSize = 0;
		bool skipTimeout = false;

		bool continueReading = false;
		bool isCallback = !std::holds_alternative<std::reference_wrapper<DataArray>>(outputOrCallback);
		DataArray &output = isCallback ? dataBuffer : std::get<std::reference_wrapper<DataArray>>(outputOrCallback);
		const std::function<bool(const DataArray &)> *callback = isCallback ? &std::get<std::function<bool(const DataArray &)>>(outputOrCallback) : nullptr;

		/* Make sure that data output's size matches "inputReportLength". */
		if (output.size() != this->deviceInfo->inputReportLength)
		{
			output.resize(this->deviceInfo->inputReportLength);
		}

		do
		{
			if (!this->readIsPending)
			{
				this->readIsPending = false;
				result = ResetEvent(this->olRead.get().hEvent);

				if (!result)
				{
					return GetLastError();
				}

				/* Windows' docs say that "GetOverlappedResult" function should be used
				to retrieve read size for OVERLAPPED handle instead of "ReadFile". */
				result = ReadFile(
					this->deviceHandle.get(),
					output.data(),
					static_cast<DWORD>(output.size()),
					NULL,
					&const_cast<OVERLAPPED &>(this->olRead.get()));

				if (!result)
				{
					/* "ERROR_IO_PENDING" is not an error, however other codes are. */
					DWORD errorCode = GetLastError();
					if (errorCode != ERROR_IO_PENDING)
					{
						CancelIoEx(this->deviceHandle.get(), &const_cast<OVERLAPPED &>(this->olRead.get()));
						return this->handleDeviceClose(errorCode);
					}
				}
				else
				{
					skipTimeout = true;
				}
			}

			if (timeout && !skipTimeout)
			{
				DWORD time = static_cast<DWORD>(timeout.value().count());
				DWORD waitResult = WaitForSingleObject(this->olRead.get().hEvent, time);

				switch (waitResult)
				{
				case WAIT_FAILED:
				{ // Waiting has failed for some reason - propagate error to user;
					DWORD errorCode = GetLastError();

					CancelIoEx(this->deviceHandle.get(), &const_cast<OVERLAPPED &>(this->olRead.get()));
					this->readIsPending = false;

					return this->handleDeviceClose(errorCode);
				}
				case WAIT_TIMEOUT:
				{ // Wait has timed out - return 0;
					this->readIsPending = true;
					return errc::OK;
				}
				default: /* WAIT_OBJECT_0. Wait was successful. */
					this->readIsPending = false;
					break;
				}
			}

			result = GetOverlappedResult(
				this->deviceHandle.get(),
				&const_cast<OVERLAPPED &>(this->olRead.get()),
				&readSize,
				TRUE);

			if (!result)
			{
				return this->handleDeviceClose(GetLastError());
			}

			if (readSize > 0)
			{
				if (output[0] == std::byte{0x00})
				{
					/* According to https://github.com/signal11/hidapi/blob/master/windows/hid.c, Windows
					likes to prepend 0 byte when report numbers are not used. That is not cross-platform, thus
					it needs to be removed. */
					std::rotate(output.begin(), output.begin() + 1, output.end());
					readSize--;
				}
			}
			output.resize(readSize);

			if (isCallback)
			{
				if ((*callback)(output))
				{
					continueReading = false;
				}
			}
		} while (isCallback && continueReading);

		return errc::OK;
	}

	HidResult write(const DataArray &data)
	{
		std::unique_lock writeLock(this->writeMutex, std::defer_lock);

		{
			std::lock_guard lock(this->mutex);

			if (!this->isOpen())
			{
				return errc::DEVICE_IS_CLOSED;
			}
			else if (!writeLock.try_lock())
			{
				return errc::BUSY;
			}
		}

		BOOL result = FALSE;
		DWORD writeSize = 0;
		DataArray temporaryData;
		DataArray &outputData = const_cast<DataArray &>(data);

		/* Make sure that data vector's size matches "outputReportLength". */
		if (data.size() < this->deviceInfo->outputReportLength)
		{
			temporaryData.resize(this->deviceInfo->outputReportLength);
			std::copy(data.begin(), data.end(), temporaryData.begin());
			outputData = temporaryData;
		}
		else if (data.size() > this->deviceInfo->outputReportLength)
		{
			return errc::OUTPUT_IS_TOO_BIG;
		}

		result = ResetEvent(this->olWrite.get().hEvent);

		if (!result)
		{
			return GetLastError();
		}

		result = WriteFile(
			this->deviceHandle.get(),
			outputData.data(),
			static_cast<DWORD>(outputData.size()),
			NULL,
			&const_cast<OVERLAPPED &>(this->olWrite.get()));

		if (!result)
		{
			/* "ERROR_IO_PENDING" is not an error, however other codes are. */
			DWORD errorCode = GetLastError();
			if (errorCode != ERROR_IO_PENDING)
			{
				CancelIoEx(this->deviceHandle.get(), &const_cast<OVERLAPPED &>(this->olWrite.get()));
				return this->handleDeviceClose(errorCode);
			}
		}

		result = GetOverlappedResult(
			this->deviceHandle.get(),
			&const_cast<OVERLAPPED &>(this->olWrite.get()),
			&writeSize,
			TRUE);

		if (!result)
		{
			return this->handleDeviceClose(GetLastError());
		}

		return errc::OK;
	}

	HidResult getFeatureReport(DataArray &output)
	{
		std::unique_lock getLock(this->getMutex, std::defer_lock);

		{
			std::lock_guard lock(this->mutex);

			if (!this->isOpen())
			{
				return errc::DEVICE_IS_CLOSED;
			}
			else if (!getLock.try_lock())
			{
				return errc::BUSY;
			}
		}

		BOOL result = FALSE;
		DWORD readSize = 0;

		/* Resize output to "featureReportLength". */
		if (output.size() != this->deviceInfo->featureReportLength)
		{
			output.resize(this->deviceInfo->featureReportLength);
		}

		result = ResetEvent(this->olGet.get().hEvent);

		if (!result)
		{
			return GetLastError();
		}

		result = DeviceIoControl(
			this->deviceHandle.get(),
			IOCTL_HID_GET_FEATURE,
			output.data(), static_cast<DWORD>(output.size()),
			output.data(), static_cast<DWORD>(output.size()),
			nullptr,
			&const_cast<OVERLAPPED &>(this->olGet.get()));

		if (!result)
		{
			/* "ERROR_IO_PENDING" is not an error, however other codes are. */
			DWORD errorCode = GetLastError();
			if (errorCode != ERROR_IO_PENDING)
			{
				CancelIoEx(this->deviceHandle.get(), &const_cast<OVERLAPPED &>(this->olGet.get()));
				return this->handleDeviceClose(errorCode);
			}
		}

		result = GetOverlappedResult(
			this->deviceHandle.get(),
			&const_cast<OVERLAPPED &>(this->olGet.get()),
			&readSize,
			TRUE);

		if (!result)
		{
			return this->handleDeviceClose(GetLastError());
		}

		/* read data does not include report id, thus we increase readSize by 1. */
		if (++readSize != output.size())
		{
			output.resize(readSize);
		}

		return errc::OK;
	}

	HidResult setFeatureReport(const DataArray &data)
	{
		std::unique_lock setLock(this->setMutex, std::defer_lock);

		{
			std::lock_guard lock(this->mutex);

			if (!this->isOpen())
			{
				return errc::DEVICE_IS_CLOSED;
			}
			else if (!setLock.try_lock())
			{
				return errc::BUSY;
			}
		}

		BOOL result = FALSE;
		DWORD writeSize = 0;
		DataArray temporaryData;
		DataArray &outputData = const_cast<std::vector<std::byte> &>(data);

		/* Make sure that data vector's size matches "featureReportLength". */
		if (data.size() < this->deviceInfo->featureReportLength)
		{
			temporaryData.resize(this->deviceInfo->featureReportLength);
			std::copy(data.begin(), data.end(), temporaryData.begin());
			outputData = temporaryData;
		}
		else
		{
			return errc::OUTPUT_IS_TOO_BIG;
		}

		result = ResetEvent(this->olSet.get().hEvent);

		if (!result)
		{
			return GetLastError();
		}

		result = DeviceIoControl(
			this->deviceHandle.get(),
			IOCTL_HID_SET_FEATURE,
			const_cast<DataArray &>(data).data(), static_cast<DWORD>(data.size()),
			nullptr, 0,
			nullptr,
			&const_cast<OVERLAPPED &>(this->olSet.get()));

		if (!result)
		{
			/* "ERROR_IO_PENDING" is not an error, however other codes are. */
			DWORD errorCode = GetLastError();
			if (errorCode != ERROR_IO_PENDING)
			{
				CancelIoEx(this->deviceHandle.get(), &const_cast<OVERLAPPED &>(this->olSet.get()));
				return this->handleDeviceClose(errorCode);
			}
		}

		result = GetOverlappedResult(
			this->deviceHandle.get(),
			&const_cast<OVERLAPPED &>(this->olSet.get()),
			&writeSize,
			TRUE);

		if (!result)
		{
			return this->handleDeviceClose(GetLastError());
		}

		return errc::OK;
	}

  private:
	bool readIsPending;
	std::optional<HidWatcher::CallbackId> callbackId;
	sr::unique_resource<HANDLE, void (*)(HANDLE)> deviceHandle;
	sr::unique_resource<OVERLAPPED, void (*)(const OVERLAPPED &)> olRead, olWrite, olSet, olGet;
	std::mutex readMutex, writeMutex, setMutex, getMutex;

	std::pair<HidResult, std::function<void(const std::string &path)>> openUnlocked(const std::string &path, bool closeIfOpen, const std::optional<std::function<void(const std::string &path)>> &onClose)
	{
		HidResult result;
		std::function<void(const std::string &path)> closeCallback;

		if (this->isOpen())
		{
			if (!closeIfOpen)
			{
				return {errc::DEVICE_IS_OPEN, closeCallback};
			}
			else if (auto [closeResult, callback] = this->closeUnlocked(); !closeResult)
			{
				return {closeResult, callback};
			}
		}

		this->deviceHandle.reset(
			CreateFileA(
				path.c_str(),
				GENERIC_WRITE | GENERIC_READ,
				FILE_SHARE_READ | FILE_SHARE_WRITE,
				NULL,
				OPEN_EXISTING,
				FILE_FLAG_OVERLAPPED,
				0));

		if (this->deviceHandle.get() == INVALID_HANDLE_VALUE)
		{
			result = GetLastError();
		}
		else if (auto deviceInfo = HidDevice::generateDeviceInfo(path); deviceInfo)
		{
			this->deviceInfo = deviceInfo.value();
			this->onClose = onClose ? *onClose : std::function<void(const std::string &path)>();
		}
		else
		{
			this->deviceHandle.reset(static_cast<void *>(INVALID_HANDLE_VALUE));
			result = deviceInfo.error();
		}

		return {result, closeCallback};
	}

	std::pair<HidResult, std::function<void(const std::string &path)>> closeUnlocked()
	{
		HidResult result;
		std::function<void(const std::string &path)> closeCallback;

		if (this->isOpen())
		{
			/* Abort all ongoing reads and writes. */
			if (!CancelIoEx(this->deviceHandle.get(), NULL))
			{
				DWORD error = GetLastError();
				if (error != ERROR_OPERATION_ABORTED)
				{
					result = GetLastError();
				}
			}

			/* Wait for all ongoing reads and writes to end. */
			std::scoped_lock locks(this->readMutex, this->writeMutex, this->getMutex, this->setMutex);
			std::string path = this->deviceInfo->path;

			this->deviceHandle.reset(static_cast<void *>(INVALID_HANDLE_VALUE));
			this->deviceInfo = std::nullopt;
			this->onClose.swap(closeCallback);

			if (this->callbackId)
			{
				HidWatcher::getInstance().removeCallback(this->callbackId.value());
				this->callbackId = std::nullopt;
			}
		}

		return {result, closeCallback};
	}

	bool isOpen() const
	{
		return this->deviceHandle.get() != INVALID_HANDLE_VALUE;
	}

	HidResult handleDeviceClose(DWORD error)
	{
		switch (error)
		{
		case ERROR_OPERATION_ABORTED:
			return errc::ABORTED;
		case ERROR_DEVICE_NOT_CONNECTED:
			this->close();
			return errc::DISCONNECTED;
		default:
			return error;
		}
	}
};
} // namespace hidcpp::internals

namespace hidcpp
{
HidDevice::HidDevice() : pImpl{std::make_unique<internals::HidDeviceWindows>()}
{
}
} // namespace hidcpp
