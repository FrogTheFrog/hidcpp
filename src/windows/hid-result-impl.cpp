#include "../../include/hid-result.hpp"
#include <Windows.h>

namespace hidcpp
{
std::string HidResult::getErrorCodeMessage(std::uint32_t systemError)
{
	if (systemError == ERROR_SUCCESS)
	{
		return std::string("No error.");
	}
	else
	{
		LPSTR buffer = nullptr;
		DWORD bufferSize = FormatMessageA(
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER,
			NULL,
			systemError,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			reinterpret_cast<LPSTR>(&buffer),
			0,
			NULL);

		if (bufferSize > 0)
		{
			auto deleter = [](auto &&buffer) {
				HeapFree(GetProcessHeap(), 0, buffer);
			};
			std::unique_ptr<CHAR, decltype(deleter)> message(buffer, deleter);
			return std::string(message.get(), bufferSize);
		}
		else
		{
			throw std::system_error(
				GetLastError(),
				std::system_category(),
				"Failed to retrieve error message string.");
		}
	}
}

HidResult::HidResult()
	: errorCode(hidcpp::errc::OK), systemSpecificCode(ERROR_SUCCESS)
{
}

HidResult::HidResult(hidcpp::errc errorCode)
	: errorCode(errorCode), systemSpecificCode(ERROR_SUCCESS)
{
}

HidResult::HidResult(std::uint32_t osCode)
	: errorCode(hidcpp::errc::SYSTEM_ERROR), systemSpecificCode(osCode)
{
}
} // namespace hidcpp
