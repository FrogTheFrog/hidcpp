#include "./include/hid-dll-loader.hpp"
#include "../../include/hid-result.hpp"
#include <stdexcept>

namespace hidcpp::internals
{
HidDllLoader &HidDllLoader::get()
{
	static HidDllLoader instance;
	return instance;
}

HidDllLoader::HidDllLoader() : dllHandle{LoadLibraryA("hid.dll"), [](HMODULE dll) { if (dll != NULL) FreeLibrary(dll); }}
{
	if (dllHandle.get() == NULL)
	{
		throw std::runtime_error(HidResult::getErrorCodeMessage(GetLastError()));
	}

#define RESOLVE(x)                                                   \
	x = reinterpret_cast<x##_>(GetProcAddress(dllHandle.get(), #x)); \
	if (!x)                                                          \
		throw std::exception("Failed to load \"" #x "\" function.");

	RESOLVE(HidD_GetAttributes);
	RESOLVE(HidD_GetPreparsedData);
	RESOLVE(HidD_FreePreparsedData);
	RESOLVE(HidP_GetCaps);
	RESOLVE(HidD_GetSerialNumberString);
	RESOLVE(HidD_GetManufacturerString);
	RESOLVE(HidD_GetProductString);

#undef RESOLVE
}
} // namespace hidcpp::internals