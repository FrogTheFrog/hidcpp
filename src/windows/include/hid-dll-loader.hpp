#pragma once

#include "../../../include/scope.hpp"
#include <Windows.h>

namespace hidcpp::internals
{
class HidDllLoader
{
  public:
	typedef void *PHIDP_PREPARSED_DATA;
	typedef USHORT USAGE;

	typedef struct
	{
		ULONG size;
		USHORT vendorId;
		USHORT productId;
		USHORT versionNumber;
	} HIDD_ATTRIBUTES, *PHIDD_ATTRIBUTES;

	typedef struct _HIDP_CAPS
	{
		USAGE usage;
		USAGE usagePage;
		USHORT inputReportByteLength;
		USHORT outputReportByteLength;
		USHORT featureReportByteLength;
		USHORT reserved[17];
		USHORT notUsed[10];
	} HIDP_CAPS, *PHIDP_CAPS;

	typedef BOOLEAN(__stdcall *HidD_GetAttributes_)(
		HANDLE device, PHIDD_ATTRIBUTES attrib);
	typedef BOOLEAN(__stdcall *HidD_GetPreparsedData_)(
		HANDLE handle, PHIDP_PREPARSED_DATA *preparsed_data);
	typedef BOOLEAN(__stdcall *HidD_FreePreparsedData_)(
		PHIDP_PREPARSED_DATA preparsed_data);
	typedef NTSTATUS(__stdcall *HidP_GetCaps_)(
		PHIDP_PREPARSED_DATA preparsed_data, HIDP_CAPS *caps);
	typedef BOOLEAN(__stdcall *HidD_GetSerialNumberString_)(
		HANDLE device, PVOID buffer, ULONG bufferLength);
	typedef BOOLEAN(__stdcall *HidD_GetManufacturerString_)(
		HANDLE handle, PVOID buffer, ULONG bufferLength);
	typedef BOOLEAN(__stdcall *HidD_GetProductString_)(
		HANDLE handle, PVOID buffer, ULONG bufferLength);

	HidD_GetAttributes_ HidD_GetAttributes;
	HidD_GetPreparsedData_ HidD_GetPreparsedData;
	HidD_FreePreparsedData_ HidD_FreePreparsedData;
	HidP_GetCaps_ HidP_GetCaps;
	HidD_GetSerialNumberString_ HidD_GetSerialNumberString;
	HidD_GetManufacturerString_ HidD_GetManufacturerString;
	HidD_GetProductString_ HidD_GetProductString;

	static HidDllLoader &get();

  private:
	sr::unique_resource<HMODULE, void (*)(HMODULE)> dllHandle;

	HidDllLoader();
};
} // namespace hidcpp::internals