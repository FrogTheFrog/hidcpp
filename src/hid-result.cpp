#include "../include/hid-result.hpp"

namespace hidcpp
{
errc HidResult::code() const
{
	return this->errorCode;
}

std::uint32_t HidResult::systemCode() const
{
	return this->systemSpecificCode;
}

HidResult::operator bool() const
{
	return this->errorCode == errc::OK;
}

std::string HidResult::message() const
{
	return std::move(this->errorCode == errc::SYSTEM_ERROR ? this->getErrorCodeMessage(this->systemSpecificCode) : std::string{this->getErrorCodeMessage(this->errorCode)});
}
} // namespace hidcpp
