#pragma once

#include <string>
#include <string_view>

namespace hidcpp
{
enum class errc : uint8_t
{
	ABORTED,
	BUSY,
	DEVICE_IS_CLOSED,
	DEVICE_IS_OPEN,
	DISCONNECTED,
	OK,
	OUTPUT_IS_TOO_BIG,
	SYSTEM_ERROR,
};

class HidResult
{
  public:
	static std::string getErrorCodeMessage(std::uint32_t systemCode);
	static constexpr std::string_view getErrorCodeMessage(errc code)
	{
		switch (code)
		{
		case hidcpp::errc::ABORTED:
			return "Operation has been manually aborted.";
		case hidcpp::errc::BUSY:
			return "Other thread is already executing requested operation.";
		case hidcpp::errc::DEVICE_IS_CLOSED:
			return "Device is closed.";
		case hidcpp::errc::DEVICE_IS_OPEN:
			return "Device is open.";
		case hidcpp::errc::DISCONNECTED:
			return "Device has been disconnected/removed.";
		case hidcpp::errc::OK:
			return "No error.";
		case hidcpp::errc::OUTPUT_IS_TOO_BIG:
			return "Provided output data size is bigger than required output data size.";
		case hidcpp::errc::SYSTEM_ERROR:
			return "Os specific error.";
		default:
			return "Error message not set!";
		}
	}
	static constexpr std::string_view getErrorCodeAsString(errc code)
	{
		switch (code)
		{
		case hidcpp::errc::ABORTED:
			return "ABORTED";
		case hidcpp::errc::BUSY:
			return "BUSY";
		case hidcpp::errc::DEVICE_IS_CLOSED:
			return "DEVICE_IS_CLOSED";
		case hidcpp::errc::DEVICE_IS_OPEN:
			return "DEVICE_IS_OPEN";
		case hidcpp::errc::DISCONNECTED:
			return "DISCONNECTED";
		case hidcpp::errc::OK:
			return "OK";
		case hidcpp::errc::OUTPUT_IS_TOO_BIG:
			return "OUTPUT_IS_TOO_BIG";
		case hidcpp::errc::SYSTEM_ERROR:
			return "SYSTEM_ERROR";
		default:
			return "UNDEFINED";
		}
	}

	explicit HidResult();
	HidResult(errc errorCode);
	HidResult(std::uint32_t systemCode);

	errc code() const;
	std::uint32_t systemCode() const;
	std::string message() const;
	operator bool() const;

  private:
	errc errorCode;
	std::uint32_t systemSpecificCode;
};
} // namespace hidcpp
