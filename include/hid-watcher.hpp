#pragma once

#include "./internals/hid-watcher-impl.hpp"
#include "./hid-result.hpp"
#include <functional>
#include <memory>
#include <mutex>
#include <optional>

namespace hidcpp
{
class HidWatcher
{
  private:
	std::unique_ptr<internals::HidWatcherImpl> pImpl;

  public:
	typedef internals::HidWatcherImpl::CallbackId CallbackId;

	static HidWatcher& getInstance();

	HidWatcher();
	~HidWatcher() = default;
	HidWatcher(HidWatcher &&) = default;
	HidWatcher(const HidWatcher &) = delete;
	HidWatcher &operator=(HidWatcher &&) = default;
	HidWatcher &operator=(const HidWatcher &) = delete;

	CallbackId addCallback(const std::function<bool(const std::string &path, bool removed)> &callback);
	void removeCallback(CallbackId id);
	HidResult start();
	HidResult stop();
};
} // namespace hidcpp