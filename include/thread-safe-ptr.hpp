#pragma once

#include <shared_mutex>
#include <type_traits>

namespace hidcpp::internals
{
template <typename T>
class ThreadSafePtr
{
	class SafePtr
	{
		friend class ThreadSafePtr<T>;

		class LockedPtr
		{
		  public:
			LockedPtr(SafePtr &self) : self{self}, lock{self.mutex} {}
			~LockedPtr() = default;
			LockedPtr(LockedPtr &&) = default;
			LockedPtr(const LockedPtr &) = delete;
			LockedPtr &operator=(LockedPtr &&) = default;
			LockedPtr &operator=(const LockedPtr &) = delete;

			operator bool() const
			{
				return this->self.pointer != nullptr;
			}

			auto operator->()
			{
				return this->self.pointer;
			}

		  private:
			SafePtr &self;
			std::shared_lock<std::shared_mutex> lock;
		};

	  public:
		SafePtr(T *pointer) : pointer{pointer} {}
		~SafePtr() = default;
		SafePtr(SafePtr &&) = default;
		SafePtr(const SafePtr &) = delete;
		SafePtr &operator=(SafePtr &&) = default;
		SafePtr &operator=(const SafePtr &) = delete;

		LockedPtr lock()
		{
			return LockedPtr(*this);
		}

	  private:
		T *pointer;
		std::shared_mutex mutex;
	};

  public:
	ThreadSafePtr() = default;
	~ThreadSafePtr() { this->dereference(); }
	ThreadSafePtr(ThreadSafePtr &&) = default;
	ThreadSafePtr(const ThreadSafePtr &)
	{
		/* Do nothing. */
	}
	ThreadSafePtr &operator=(ThreadSafePtr &&) = default;
	ThreadSafePtr &operator=(const ThreadSafePtr &)
	{
		return *this;
	}

  protected:
	std::shared_ptr<SafePtr> getSafePtr()
	{
		if (!this->safePtr)
		{
			this->safePtr = std::make_shared<SafePtr>(static_cast<T*>(this));
		}
		return this->safePtr;
	}

	void dereference()
	{
		if (this->safePtr)
		{
			std::lock_guard lock(this->safePtr->mutex);
			this->safePtr->pointer = nullptr;
		}
	}

  private:
	std::shared_ptr<SafePtr> safePtr;
};
} // namespace hidcpp::internals
