#pragma once

#include "./data-types.hpp"
#include "./hid-result.hpp"
#include "./hid-watcher.hpp"
#include "./internals/hid-device-impl.hpp"
#include <chrono>
#include <functional>
#include <memory>
#include <optional>
#include <variant>
#include <vector>

namespace hidcpp
{
class HidDevice
{
  private:
	std::unique_ptr<internals::HidDeviceImpl> pImpl;

  public:
	typedef internals::HidDeviceImpl::DataArray DataArray;

	static HidResult getDeviceStatus(const std::string &path);
	static tl::expected<DeviceInfo, HidResult> generateDeviceInfo(const std::string &path);
	static tl::expected<EnumerationData, HidResult> enumerate();
	static tl::expected<EnumerationData, HidResult> enumerate(const std::function<bool(const std::string &)> &predicate);

	HidDevice();
	~HidDevice() = default;
	HidDevice(HidDevice &&) = default;
	HidDevice(const HidDevice &) = delete;
	HidDevice &operator=(HidDevice &&) = default;
	HidDevice &operator=(const HidDevice &) = delete;

	HidResult open(const std::string &path, bool closeIfOpen, const std::optional<std::function<void(const std::string &path)>> &onClose = std::nullopt);
	HidResult close();

	std::optional<DeviceInfo> getDeviceInfo() const;

	HidResult abortReading();

	HidResult read(const std::variant<std::reference_wrapper<DataArray>, std::function<bool(const DataArray &)>> &outputOrCallback, const std::optional<std::chrono::milliseconds> &timeout = std::nullopt);
	HidResult write(const DataArray &data);

	HidResult getFeatureReport(DataArray &output);
	HidResult setFeatureReport(const DataArray &data);
};
} // namespace hidcpp