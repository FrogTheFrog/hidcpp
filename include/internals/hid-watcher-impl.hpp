#pragma once

#include "../hid-result.hpp"
#include <mutex>

namespace hidcpp
{
class HidWatcher;
} // namespace hidcpp

namespace hidcpp::internals
{
class HidWatcherImpl
{
  public:
	typedef std::size_t CallbackId;

	HidWatcherImpl() = default;
	virtual ~HidWatcherImpl() = default;
	HidWatcherImpl(HidWatcherImpl &&) = default;
	HidWatcherImpl(const HidWatcherImpl &) = delete;
	virtual HidWatcherImpl &operator=(HidWatcherImpl &&) = default;
	HidWatcherImpl &operator=(const HidWatcherImpl &) = delete;

	virtual HidResult start() = 0;
	virtual HidResult stop() = 0;
	virtual CallbackId addCallback(const std::function<bool(const std::string &path, bool removed)> &callback) = 0;
	virtual void removeCallback(CallbackId id) = 0;

  protected:
	mutable std::mutex mutex;
	std::unordered_map<CallbackId, std::function<bool(const std::string &path, bool removed)>> callbacks;
};
} // namespace hidcpp::internals