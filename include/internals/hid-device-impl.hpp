#pragma once

#include "../data-types.hpp"
#include "../expected.hpp"
#include "../hid-result.hpp"
#include "./hid-watcher.hpp"
#include <chrono>
#include <functional>
#include <memory>
#include <mutex>
#include <optional>
#include <variant>
#include <vector>

namespace hidcpp
{
class HidDevice;
}

namespace hidcpp::internals
{
class HidDeviceImpl
{
  public:
	typedef std::vector<std::byte> DataArray;

	HidDeviceImpl() = default;
	virtual ~HidDeviceImpl() = default;
	HidDeviceImpl(HidDeviceImpl &&) = default;
	HidDeviceImpl(const HidDeviceImpl &) = delete;
	virtual HidDeviceImpl &operator=(HidDeviceImpl &&) = default;
	HidDeviceImpl &operator=(const HidDeviceImpl &) = delete;

	virtual HidResult open(const std::string &path, bool closeIfOpen, const std::optional<std::function<void(const std::string &path)>> &onClose) = 0;
	virtual HidResult close() = 0;

	virtual std::optional<DeviceInfo> getDeviceInfo() const = 0;

	virtual HidResult abortReading() = 0;

	virtual HidResult read(const std::variant<std::reference_wrapper<DataArray>, std::function<bool(const DataArray &)>> &outputOrCallback, const std::optional<std::chrono::milliseconds> &timeout = std::nullopt) = 0;
	virtual HidResult write(const DataArray &data) = 0;

	virtual HidResult getFeatureReport(DataArray &output) = 0;
	virtual HidResult setFeatureReport(const DataArray &data) = 0;

  protected:
	mutable std::mutex mutex;
	std::optional<DeviceInfo> deviceInfo;
	std::function<void(const std::string &path)> onClose;
};
} // namespace hidcpp::internals