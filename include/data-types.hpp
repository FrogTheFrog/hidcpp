#pragma once

#include "./hid-result.hpp"
#include <cinttypes>
#include <optional>
#include <string>
#include <vector>

namespace hidcpp
{
struct DeviceInfo
{
	std::string path;
	std::uint16_t vendorId;
	std::uint16_t productId;
	std::uint16_t usagePage;
	std::uint16_t usage;
	std::uint16_t versionNumber;
	std::uint16_t inputReportLength;
	std::uint16_t outputReportLength;
	std::uint16_t featureReportLength;
	std::optional<std::uint16_t> interfaceNumber;
	std::optional<std::string> serialNumber;
	std::optional<std::string> manufacturer;
	std::optional<std::string> product;
};
struct ErroredDevice
{
	std::string path;
	HidResult result;
};
struct EnumerationData
{
	std::vector<std::string> valid;
	std::vector<ErroredDevice> errored;
};
} // namespace hidcpp