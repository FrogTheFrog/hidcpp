cmake_minimum_required(VERSION 3.12)
project(hidcpp)
file(GLOB SHARED_SOURCE "${CMAKE_CURRENT_LIST_DIR}/src/*.cpp")

if(WIN32) #Windows
	file(GLOB PLATFORM_SOURCE "${CMAKE_CURRENT_LIST_DIR}/src/windows/*.cpp")
elseif(UNIX AND NOT APPLE) #Linux
	message(FATAL_ERROR "Only Windows platform is supported at this time." )
else() #Other
	message(FATAL_ERROR "Only Windows platform is supported at this time." )
endif()

add_library(${PROJECT_NAME} STATIC ${SHARED_SOURCE} ${PLATFORM_SOURCE})
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_STANDARD 20 CXX_STANDARD_REQUIRED ON)
target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_LIST_DIR}/include/")
